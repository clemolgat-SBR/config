#!/usr/bin/env bash

# /!\ This script won't do backup /!\
set -x
# install Docker
pacman -S --needed docker
(mkdir /home/docker && rm -rf /var/lib/docker && cd /var/lib && ln -s /home/docker .)
systemctl enable docker
systemctl start docker

# Fix dns name resolution
cp nsswitch.conf /etc/nsswitch.conf


