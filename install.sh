#!/usr/bin/env bash

# /!\ This script won't do backup /!\
set -x
yaourt -S --needed chromium\
  base base-devel git tk meld clang cmake ninja chrpath doxygen \
  qt5 qtcreator ipython \
  awesome xlockmore zathura feh wireshark-qt diffstat \
  docker python2-pip \
  unzip unrar p7zip pigz cpio \
  coreutils util-linux udevil sshpass wget e2fsprogs ncdu tree \
  minicom nfs-utils gnuplot

sudo usermod -a -G wireshark,docker $USER

cp zshrc ~/.zshrc
cp gitconfig ~/.gitconfig
cp Xdefaults ~/.Xdefaults
mkdir -pv ~/.urxvt/ext && cp resize-font ~/.urxvt/ext/
cp xinitrc ~/.xinitrc
mkdir -pv ~/.config/awesome && cp rc.lua ~/.config/awesome/

